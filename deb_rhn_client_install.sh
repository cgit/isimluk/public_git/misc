#!/bin/bash -x
#
# Summary: Install client packages for spacewalk on debian. Don't do this at home!
# Author:  Simon Lukasik <isimluk@fedoraproject.org>
# License: WTFPLv2
# Date:    2010-06-05
#


## Configuration, comment out apropriate lines
arch=i386
#arch=amd64
repo=http://download.opensuse.org/repositories/home:/xsuchy:/debian-spacewalk/Debian_5.0/
#repo=http://download.opensuse.org/repositories/home:/xsuchy:/debian-spacewalk/xUbuntu_9.10/


## Download packages
mkdir -p temp && cd $_
for file in `cat ../package.list`; do
  wget $repo$file || (echo "FAIL"; exit)
done
cd -


## Install sw-client packages
for package in temp/*all.deb; do
  dpkg -i $package
done
for package in temp/*$arch.deb; do
  dpkg -i $package
done


## Install dependencies
apt-get -f -y install

cd ..
echo "PASS."

